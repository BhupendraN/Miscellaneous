/*
CMPE 24    : Data Scrambler & De Scrambler
Author     : Bhupendra Naphade
SJSU ID    : 010703836
Description: Scrambler nd De-Scrambler algorithm implementation of any order
*/


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main()
{
setvbuf(stdout,NULL,_IONBF,0);
int order,temp,N;
int scramble [14];
int i,del3,del5;
int ip[100];
printf("Enter the order:");
scanf("%d",&order);
printf("Enter the value of N\n"); //Number of input elements
scanf("%d",&N);

printf("Enter the input stream elements one by one\n"); // Enter the binary pattern to be scrambled
for(i=0; i<N ; i++)
{
 scanf("%d",&ip[i]);
}


temp =order/2;
//scrambler
for(i=0;i<N;i++)
{
    if(i>=temp)
    {
        del3=scramble[i-temp];
    }
    else
    {
        del3 = 0;
    }
    if(i>=order)
    {
        del5=scramble[i-order];
    }
    else
    {
        del5 = 0;
    }

    scramble[i] =ip[i]^del3^del5;
}
printf("Input              : ");
for(i=0;i<N;i++)
{
    printf("%d",ip[i]);
}
printf("\nScrambled output   : ");
for(i=0;i<N;i++)
{
    printf("%d",scramble[i]);
}



int descramble [14];
del3=del5=0;
//descrambler
for(i=0;i<N;i++)
{
    if(i>=temp)
    {
        del3=scramble[i-temp];
    }
    else
    {
        del3 = 0;
    }
    if(i>=order)
    {
        del5=scramble[i-order];
    }
    else
    {
        del5 = 0;
    }

    descramble[i] =(del3^del5)^scramble[i];
}
printf("\nDescrambled output : ");
for(i=0;i<N;i++)
{
    printf("%d",descramble[i]);
}
return 0;
}
